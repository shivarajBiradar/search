package com.yml.search;

import java.util.Collection;
import java.util.LinkedList;

public class TrieNode {
char content;
boolean word_end;
Collection<TrieNode> child;

int count;

public TrieNode( char c) {
	// TODO Auto-generated constructor stub
	
	child=new LinkedList<TrieNode>();
	word_end=false;
	content=c;
	count=0;
	
	
}
public TrieNode subNode(char c)
{
	if(child!=null)
		for(TrieNode eachChild:child)
			if(eachChild.content==c)
				return eachChild;
	return null;
			
}

}
class Trie
{
	
	private TrieNode root;
	
	public Trie() {
		// TODO Auto-generated constructor stub
		root=new TrieNode(' ');
		
	}
	public void insert(String word)
	{
		if(search(word)==true)
			return;
		TrieNode current=root;
		for(char ch:word.toCharArray())
		{
			TrieNode child_One=current.subNode(ch);
			if(child_One!=null)
			{
				current=child_One;
			}else
			{
				current.child.add(new TrieNode(ch));
				current=current.subNode(ch);
				
				
				
			}
			
			current.count++;
		}
		current.word_end=true;
		
	}
    public boolean search(String word)

    {

        TrieNode current = root;  

        for (char ch : word.toCharArray() )

        {

            if (current.subNode(ch) == null)

                return false;

            else

                current = current.subNode(ch);

        }      

        if (current.word_end == true) 
        

            return true;
    	System.out.println("search found");
    	return false;
        

    }
	
}