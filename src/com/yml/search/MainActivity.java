package com.yml.search;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.util.HashSet;

import android.app.Activity;
import android.graphics.Color;

import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends Activity {

	String str;
	String[] arr;
	
	HashSet<String> set = new HashSet<String>();
	
	BufferedReader bReader;
	ArrayAdapter<String> adapter;

	// Trie newTrie = new Trie();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		try {

			bReader = new BufferedReader(new FileReader("/sdcard/newfile"));

			

			while ((str = bReader.readLine()) != null) {
				
				set.add(str);

				// newTrie.insert(str);
				Log.d("string contained", str);

			}
		} catch (FileNotFoundException e) {

		} catch (IOException e) {
			// TODO: handle exception
		}

		arr = set.toArray(new String[0]);

		// newTrie.search("hi");

		adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item, arr);
		AutoCompleteTextView text = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
		text.setThreshold(1);
		text.setAdapter(adapter);
		text.setTextColor(Color.RED);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
